; 100 wattaren
api = 2
core = 7.x

libraries[tcpdf][type] = "libraries"
libraries[tcpdf][download][type] = "file"
libraries[tcpdf][download][url] = "https://github.com/wvega/timepicker/releases/download/1.3.2/jquery-timepicker-1.3.2.zip"
libraries[tcpdf][directory_name] = "wvega-timepicker"
libraries[tcpdf][destination] = "libraries"
