<?php
/**
 * @file
 * Administrative page callbacks for the nodeflow module.
 */

/**
 * General configuration form for controlling the nodeflow behaviour.
 */
function nodeflow_admin() {
  $form = array();

  $form['nodeflow_update_created_time'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update the created time of the node to match the published time.'),
    '#default_value' => variable_get('nodeflow_update_created_time', '0'),
    '#return_value' => 1,
    '#description' => t("If checked, this will update the created time of the node to the time the node was published."),
  );

  $views_raw = views_get_all_views(TRUE);
  $views_raw =  array_filter($views_raw, 'views_view_is_enabled');

  foreach ($views_raw as $key => $value) {
    $views[$key] = $views_raw[$key]->human_name;
  }

  $form['nodeflow_view_draft'] = array(
    '#title' => t('Select the default view draft'),
    '#description' => t('Draft view is used to see node'),
    '#type' => 'select',
    '#default_value' => variable_get('nodeflow_view_draft', 'nodeflow_draft'),
    '#options' => $views,
  );

  $form['nodeflow_view_published'] = array(
    '#title' => t('Select the default view published'),
    '#description' => t('Published view is used to see node'),
    '#type' => 'select',
    '#default_value' => variable_get('nodeflow_view_published', 'nodeflow_published'),
    '#options' => $views,
  );

  // $form['nodeflow_view_viewer'] = array();

  return system_settings_form($form);
}
